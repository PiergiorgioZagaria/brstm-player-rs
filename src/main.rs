use std::{error::Error, thread, time::Duration};

use player_thread::ResponseType;

use crate::player_thread::{PlayerThread, RequestType};
mod player_thread;
mod ui;

// Esempio
struct Player {
    thread: PlayerThread,
    duration: Option<f64>,
    quit: bool,
    time_pos: f64,
}

impl Player {
    fn recv(&mut self) -> Result<(), String> {
        match self.thread.recv()? {
            ResponseType::Duration(x) => self.duration = Some(x),
            ResponseType::TimePos(x) => self.time_pos = x,
            _ => todo!(),
        }
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let thread = PlayerThread::spawn("./assets/DontDoIt.brstm".to_string())?;
    println!("Player initialized");
    let mut player = Player {
        thread,
        duration: None,
        quit: false,
        time_pos: 0.0,
    };

    player.thread.send(RequestType::GetDuration)?;
    player.recv()?;
    println!("Duration: {}", player.duration.unwrap());

    while !player.quit {
        thread::sleep(Duration::from_secs(1));
        player.thread.send(RequestType::GetTimePos)?;
        player.recv()?;
        println!("{:?}", player.time_pos);
    }

    Ok(())
}
