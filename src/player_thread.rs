use std::{
    error::Error,
    result::Result,
    sync::mpsc::{self, Receiver, Sender},
    thread::{self, JoinHandle},
};

use brstm_rs::brstm_player::BrstmPlayer;

#[derive(Debug)]
pub enum PlayerStatus {
    Playing,
    Paused,
}

#[derive(Debug)]
pub enum RequestType {
    GetCurrentChannel,
    GetDuration,
    GetLoop,
    GetPlayerStatus,
    GetPercentTimePos,
    GetTimePos,
    Ping,
    SetCurrentChannel(u8),
    SetLoop(bool),
    SetPlayerStatus(PlayerStatus),
    Seek(PlayerSeek),
    TogglePlayback,
}

#[derive(Debug)]
pub enum ResponseType {
    Success,
    Status(PlayerSeek),
    Duration(f64),
    TimePos(f64),
    // TODO Reduce to i8
    PercentagePos(i64),
    Loop(bool),
    Channel(u8),
}

#[derive(Debug)]
pub struct PlayerSeek {
    percentage: bool,
    relative: bool,
    value: f64,
}

pub struct PlayerThread {
    pub thread: JoinHandle<Result<(), String>>,
    sender: Sender<RequestType>,
    receiver: Receiver<ResponseType>,
}

impl PlayerThread {
    pub fn spawn(file: String) -> Result<Self, Box<dyn Error>> {
        let (req_sender, req_receiver) = mpsc::channel::<RequestType>();
        let (res_sender, res_receiver) = mpsc::channel::<ResponseType>();
        let (ready_sender, ready_receiver) = mpsc::channel::<bool>();
        let thread = thread::spawn(move || -> Result<(), String> {
            let mut brstm_player = BrstmPlayer::try_from(file.as_str())?;
            brstm_player.setup()?;

            ready_sender
                .send(true)
                .map_err(|e| format!("Error while sending ready confirmation: {e}"))?;

            loop {
                for recv in &req_receiver {
                    let response = match recv {
                        RequestType::GetCurrentChannel => todo!(),
                        RequestType::GetDuration => ResponseType::Duration(
                            brstm_player
                                .duration()
                                .map_err(|e| format!("Error while getting duration: {e}"))?,
                        ),
                        RequestType::GetLoop => todo!(),
                        RequestType::GetPlayerStatus => todo!(),
                        RequestType::GetPercentTimePos => ResponseType::PercentagePos(
                            brstm_player
                                .percent_pos()
                                .map_err(|e| format!("Error while getting percent pos: {e}"))?,
                        ),
                        RequestType::GetTimePos => ResponseType::TimePos(
                            brstm_player
                                .time_pos()
                                .map_err(|e| format!("Error while getting time pos: {e}"))?,
                        ),
                        RequestType::Ping => ResponseType::Success,
                        RequestType::SetCurrentChannel(_) => todo!(),
                        RequestType::SetLoop(_) => todo!(),
                        RequestType::SetPlayerStatus(s) => {
                            match s {
                                PlayerStatus::Paused => brstm_player
                                    .unpause()
                                    .map_err(|e| format!("Error while pausing: {e}"))?,
                                PlayerStatus::Playing => brstm_player
                                    .pause()
                                    .map_err(|e| format!("Error while unpausing: {e}"))?,
                            };
                            ResponseType::Success
                        }
                        RequestType::Seek(s) => {
                            if s.relative {
                                if s.percentage {
                                    let percent: isize = s.value.abs() as isize
                                        * if s.value.is_sign_negative() { -1 } else { 1 };
                                    brstm_player
                                        .seek_percent_relative(percent)
                                        .map_err(|e| format!("Error while seeking: {e}"))?;
                                } else {
                                    brstm_player
                                        .seek_relative(s.value)
                                        .map_err(|e| format!("Error while seeking: {e}"))?;
                                }
                            } else if s.percentage {
                                let percent: usize = s.value.abs() as usize;
                                brstm_player
                                    .seek_percent_absolute(percent)
                                    .map_err(|e| format!("Error while seeking: {e}"))?;
                            } else {
                                brstm_player
                                    .seek_absolute(s.value)
                                    .map_err(|e| format!("Error while seeking: {e}"))?;
                            }
                            ResponseType::Success
                        }
                        RequestType::TogglePlayback => {
                            brstm_player
                                .toggle_playback()
                                .map_err(|e| format!("Error while toggling playback: {e}"))?;
                            ResponseType::Success
                        }
                    };
                    res_sender
                        .send(response)
                        .map_err(|e| format!("Error while toggling playback: {e}"))?;
                }
            }
        });

        ready_receiver.recv()?;

        Ok(PlayerThread {
            thread,
            sender: req_sender,
            receiver: res_receiver,
        })
    }

    pub fn send(&self, request: RequestType) -> Result<(), String> {
        self.sender
            .send(request)
            .map_err(|e| format!("Error while sending request: {e}"))
    }

    pub fn recv(&self) -> Result<ResponseType, String> {
        self.receiver
            .recv()
            .map_err(|e| format!("Error while receiving request: {e}"))
    }
}
